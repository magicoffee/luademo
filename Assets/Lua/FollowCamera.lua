require "Global"

FollowCamera = {}
local this = FollowCamera
local gameObject
local transform
local target
print "FollowCamera"

function FollowCamera.Awake()
	-- body
	print("FollowCamera.Awake")
end

function FollowCamera.Start(  )
	-- body
	print("FollowCamera.Start")
end

function FollowCamera.OnEnable( )
	-- body
	print("FollowCamera.OnEnable")
	this.target = GameObject.FindWithTag("Player").transform
	print("Find Player: ", this.target.position)
end

function FollowCamera.Update( deltaTime )
	-- body
	--print("FollowCamera.Update", deltaTime)
	if this.target ~= nil then
		local offset = this.target.position
		offset:Add(Vector3.New(-0.6, 4.5, -2))
		local cameraVelocity = Vector3.zero
		this.transform.position = Vector3.SmoothDamp(this.transform.position, offset, cameraVelocity, 0.01)
		this.transform:LookAt(this.target.transform)
		print("FollowCamera: FOLLOW")
	end
end

function FollowCamera.LateUpdate(  )
	-- body
end

function FollowCamera.FixedUpdate( deltaTime )
	-- body
end

function FollowCamera.OnDisable(  )
	-- body
	print("FollowCamera.OnDisable")
end