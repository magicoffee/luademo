require "Global"

PlayerController = {}
local this = PlayerController
local gameObject
local transform

local characterController

print("Hello PlayerController")

function PlayerController.TestCo()
	print("a simple coroutine test")
	print("current time:"..Time.time)
	coroutine.wait(1)
	print("sleep time:"..Time.time)
	print("current frame:"..Time.frameCount)
	coroutine.step()
	print("yield frame:"..Time.frameCount)		
	print("coroutine over")
end

function PlayerController.Awake()
	-- body
	print("PlayerController.Awake")
end

function PlayerController.Start()
	print("PlayerController Start")
	local anim = this.gameObject:GetComponent("Animation")
	anim.wrapMode = WrapMode.Loop
	anim:Play("run01")

	local pc = this.gameObject:GetComponent("PlayerController")
	pc:PrintInfo()

	this.characterController = this.gameObject:GetComponent("CharacterController")

	coroutine.start(this.TestCo)
end

function PlayerController.Update(deltaTime)
	-- body
	--print("PlayerController.Update", deltaTime)
	local x = Input.GetAxis("Horizontal")
	local z = Input.GetAxis("Vertical")
	--this.transform:Translate(x, 0, z)
	this.transform:Rotate(0, x * 3.0, 0)
	local forward = this.transform:TransformDirection(Vector3.forward)
	this.characterController:SimpleMove(forward * z * 3.0)
end

function PlayerController.LateUpdate(  )
	-- body
end

function PlayerController.FixedUpdate( deltaTime )
	-- body
end

function PlayerController.OnEnable()
	-- body
	print("PlayerController.OnEnable")
end

function PlayerController.OnDisable()
	-- body
	print("PlayerController.OnDisable")
end