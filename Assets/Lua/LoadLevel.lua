require "Global"

LoadLevel = {}
local this = LoadLevel
local gameObject
local transform

print("Hello World")

function LoadLevel.Awake()
	-- body
	print("LoadLevel.Awake")
end

function LoadLevel.Start(  )
	-- body
	print("LoadLevel.Start")
end

function LoadLevel.OnEnable(  )
	-- body
	print("LoadLevel.OnEnable")
end

function LoadLevel.Update( deltaTime )
	-- body
	print("LoadLevel.Update", deltaTime)
end

function LoadLevel.LateUpdate(  )
	-- body
end

function LoadLevel.FixedUpdate( deltaTime )
	-- body
end

function LoadLevel.OnDisable(  )
	-- body
	print("LoadLevel.OnDisable")
end

function LoadLevel.LoadLevel(levelName)
	Application.LoadLevel(levelName)
end