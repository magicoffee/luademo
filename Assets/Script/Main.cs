﻿using UnityEngine;
using System.Collections;

public class Main : MonoBehaviour 
{
    private Framework framework = null;

    void Awake()
    {
		framework = Framework.Instance;
		DontDestroyOnLoad (gameObject);
    }
	
	// Update is called once per frame
	void Update() 
    {
        framework.Update(Time.deltaTime);
	}

	void LateUpdate()
    {
        framework.LateUpdate();
    }

	void FixedUpdate()
    {
        framework.FixedUpdate(Time.deltaTime);
    }
}
