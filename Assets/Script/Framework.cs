﻿using UnityEngine;
using System.Collections;
using LuaInterface;

public class Framework 
{
    private static Framework _instance = null;
    private LuaScriptMgr uLuaMgr = null;

	private static object lockObj = new object ();
	public static Framework Instance
	{
		get
		{
			if (_instance == null)
			{
				lock (lockObj)
				{
					if (_instance == null)
					{
						_instance = new Framework();
						_instance.Start();
					}
				}
			}
			return _instance;
		}
	}

	public LuaScriptMgr LuaScriptMgr
	{
		get
		{
			return Instance.uLuaMgr;
		}
	}

	// Use this for initialization
	public void Start()
    {
        uLuaMgr = new LuaScriptMgr();
        uLuaMgr.Start();
	}
	
	// Update is called once per frame
	public void Update(float deltaTime)
    {
        uLuaMgr.Update();
	}

    public void FixedUpdate(float deltaTime)
    {
        uLuaMgr.FixedUpdate();
    }

    public void LateUpdate()
    {
        uLuaMgr.LateUpate();
    }
}
