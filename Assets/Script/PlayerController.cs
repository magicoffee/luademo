﻿using UnityEngine;
using System.Collections;

public class PlayerController : BaseLua
{

	// Use this for initialization
	public override void Start ()
    {
        base.Start();
	}
	
	// Update is called once per frame
	public override void Update () {
		base.Update ();
	}

    public void PrintInfo()
    {
        Debug.Log("[PlayerController] Call C# method from lua script");
    }
}
