﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoadLevelButton : BaseLua
{
    public string onClickMethod;
    public string levelName;

	// Use this for initialization
	public override void Start ()
    {
        base.Start();

	    GetComponent<Button>().onClick.AddListener(
            delegate()
            {
                OnClickEvent(gameObject);
            }
        );
	}
	
	// Update is called once per frame
	public override void Update () {
	
	}

    void OnClickEvent(GameObject go)
    {
        CallMethod(onClickMethod, levelName);
    }
}
