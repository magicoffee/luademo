﻿using UnityEngine;
using System.Collections;
using LuaInterface;

public class BaseLua : MonoBehaviour 
{
    public string luaName;

	public virtual void Awake()
	{
		Framework.Instance.LuaScriptMgr.DoFile(luaName);
		LuaState l = Framework.Instance.LuaScriptMgr.lua;
		l [luaName + ".transform"] = transform;
		l [luaName + ".gameObject"] = gameObject;

		CallMethod ("Awake");
	}

    public virtual void Start()
    {
		CallMethod ("Start");
    }

	public virtual void OnEnable()
	{
		CallMethod ("OnEnable");
	}

	public virtual void Update()
	{
		CallMethod ("Update", Time.deltaTime);
	}

	public virtual void LateUpdate()
	{
		CallMethod ("LateUpdate");
	}

	public virtual void FixedUpdate()
	{
		CallMethod ("FixedUpdate", Time.deltaTime);
	}

	public virtual void OnDisable()
	{
		CallMethod ("OnDisable");
	}

    #region Call Lua Function
	protected object[] CallMethod(string func)
    {
        if (Framework.Instance.LuaScriptMgr == null)
        {
            return null;
        }
        string funcName = luaName + "." + func;
        funcName = funcName.Replace("(Clone)", "");
		return Framework.Instance.LuaScriptMgr.CallLuaFunction(funcName);
    }

    protected object[] CallMethod(string func, GameObject go)
    {
		if (Framework.Instance.LuaScriptMgr == null)
        {
            return null;
        }
        string funcName = luaName + "." + func;
        funcName = funcName.Replace("(Clone)", "");
		return Framework.Instance.LuaScriptMgr.CallLuaFunction(funcName, go);
    }

    protected object[] CallMethod(string func, params object[] args)
    {
		if (Framework.Instance.LuaScriptMgr == null)
        {
            return null;
        }
        string funcName = luaName + "." + func;
        funcName = funcName.Replace("(Clone)", "");
		return Framework.Instance.LuaScriptMgr.CallLuaFunction(funcName, args);
    }
    #endregion Call Lua Function
}
