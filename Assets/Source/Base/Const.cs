﻿
public class Const {   
    public static bool UsePbc = true;                           //PBC
    public static bool UseLpeg = true;                          //lpeg
    public static bool UsePbLua = true;                         //Protobuff-lua-gen
    public static bool UseCJson = true;                         //CJson
    public static bool UseSQLite = true;                        //SQLite
}
