﻿using System;
using UnityEngine;
using LuaInterface;

public class AssetBundleRequestWrap
{
	public static LuaMethod[] regs = new LuaMethod[]
	{
		new LuaMethod("New", _CreateAssetBundleRequest),
		new LuaMethod("GetClassType", GetClassType),
	};

	static LuaField[] fields = new LuaField[]
	{
		new LuaField("asset", get_asset, null),
		new LuaField("allAssets", get_allAssets, null),
	};

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int _CreateAssetBundleRequest(IntPtr L)
	{
		int count = LuaDLL.lua_gettop(L);

		if (count == 0)
		{
			AssetBundleRequest obj = new AssetBundleRequest();
			LuaScriptMgr.PushObject(L, obj);
			return 1;
		}
		else
		{
			LuaDLL.luaL_error(L, "invalid arguments to method: AssetBundleRequest.New");
		}

		return 0;
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int GetClassType(IntPtr L)
	{
		LuaScriptMgr.Push(L, typeof(AssetBundleRequest));
		return 1;
	}

	public static void Register(IntPtr L)
	{
		LuaScriptMgr.RegisterLib(L, "UnityEngine.AssetBundleRequest", typeof(AssetBundleRequest), regs, fields, typeof(UnityEngine.AsyncOperation));
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_asset(IntPtr L)
	{
		object o = LuaScriptMgr.GetLuaObject(L, 1);
		AssetBundleRequest obj = (AssetBundleRequest)o;

		if (obj == null)
		{
			LuaTypes types = LuaDLL.lua_type(L, 1);

			if (types == LuaTypes.LUA_TTABLE)
			{
				LuaDLL.luaL_error(L, "unknown member name asset");
			}
			else
			{
				LuaDLL.luaL_error(L, "attempt to index asset on a nil value");
			}
		}

		LuaScriptMgr.Push(L, obj.asset);
		return 1;
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_allAssets(IntPtr L)
	{
		object o = LuaScriptMgr.GetLuaObject(L, 1);
		AssetBundleRequest obj = (AssetBundleRequest)o;

		if (obj == null)
		{
			LuaTypes types = LuaDLL.lua_type(L, 1);

			if (types == LuaTypes.LUA_TTABLE)
			{
				LuaDLL.luaL_error(L, "unknown member name allAssets");
			}
			else
			{
				LuaDLL.luaL_error(L, "attempt to index allAssets on a nil value");
			}
		}

		LuaScriptMgr.PushArray(L, obj.allAssets);
		return 1;
	}
}

