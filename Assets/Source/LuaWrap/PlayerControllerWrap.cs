﻿using System;
using UnityEngine;
using LuaInterface;
using Object = UnityEngine.Object;

public class PlayerControllerWrap
{
	public static LuaMethod[] regs = new LuaMethod[]
	{
		new LuaMethod("Start", Start),
		new LuaMethod("Update", Update),
		new LuaMethod("PrintInfo", PrintInfo),
		new LuaMethod("New", _CreatePlayerController),
		new LuaMethod("GetClassType", GetClassType),
		new LuaMethod("__eq", Lua_Eq),
	};

	static LuaField[] fields = new LuaField[]
	{
	};

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int _CreatePlayerController(IntPtr L)
	{
		LuaDLL.luaL_error(L, "PlayerController class does not have a constructor function");
		return 0;
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int GetClassType(IntPtr L)
	{
		LuaScriptMgr.Push(L, typeof(PlayerController));
		return 1;
	}

	public static void Register(IntPtr L)
	{
		LuaScriptMgr.RegisterLib(L, "PlayerController", typeof(PlayerController), regs, fields, typeof(BaseLua));
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int Start(IntPtr L)
	{
		LuaScriptMgr.CheckArgsCount(L, 1);
		PlayerController obj = LuaScriptMgr.GetUnityObject<PlayerController>(L, 1);
		obj.Start();
		return 0;
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int Update(IntPtr L)
	{
		LuaScriptMgr.CheckArgsCount(L, 1);
		PlayerController obj = LuaScriptMgr.GetUnityObject<PlayerController>(L, 1);
		obj.Update();
		return 0;
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int PrintInfo(IntPtr L)
	{
		LuaScriptMgr.CheckArgsCount(L, 1);
		PlayerController obj = LuaScriptMgr.GetUnityObject<PlayerController>(L, 1);
		obj.PrintInfo();
		return 0;
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int Lua_Eq(IntPtr L)
	{
		LuaScriptMgr.CheckArgsCount(L, 2);
		Object arg0 = LuaScriptMgr.GetVarObject(L, 1) as Object;
		Object arg1 = LuaScriptMgr.GetVarObject(L, 2) as Object;
		bool o = arg0 == arg1;
		LuaScriptMgr.Push(L, o);
		return 1;
	}
}

